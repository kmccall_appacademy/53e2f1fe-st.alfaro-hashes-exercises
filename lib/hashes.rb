require 'byebug'
# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  word_lengths = Hash.new(0)
  str.split.each do |word|
    word_lengths[word] = word.length
  end
  word_lengths
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by { |_k, v| v }[-1][0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each { |k, v| older[k] = v}
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  freq = Hash.new(0)
  word.chars.each { |char| freq[char] += 1 }
  freq
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  hash = {}
  arr.each { |element| hash[element] = 0 }
  hash.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  freq = { even: 0, odd: 0}
  numbers.each do |element|
    if element.odd?
      freq[:odd] += 1
    else
      freq[:even] += 1
    end
  end
  freq
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowel_hash = { a: 0, e: 0, i: 0, o: 0, u: 0 }
  string.chars do |char|
    next unless vowel_hash[char.downcase.to_sym]
    vowel_hash[char.downcase.to_sym] += 1
  end
  max_freq = 0
  max_freq_vowel = nil
  vowel_hash.each do |k, v|
    if v > max_freq
      max_freq = v
      max_freq_vowel = k.to_s
    end
  end
  max_freq_vowel
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  selected_students = students.select { |_k, v| v > 6 }.keys
  combinations = []
  selected_students.each_with_index do |student, index|
    break if index == selected_students.length - 1
    selected_students.each_with_index do |next_student, next_index|
      next if next_index <= index
      combinations << [student, next_student]
    end
  end
  combinations
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  smallest_population_size = specimens.length
  largest_population_size = 0
  biodiversity = specimens.reduce(Hash.new(0)) do |hash, specimen|
    hash[specimen] += 1
    current_specimen_val = hash[specimen]
    if current_specimen_val > largest_population_size
      largest_population_size = current_specimen_val
    end

    if current_specimen_val < smallest_population_size
      smallest_population_size = current_specimen_val
    end
    hash
  end
  biodiversity.length**2 * (smallest_population_size / largest_population_size.to_f).ceil
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_sign_w_count = character_count(normal_sign)
  vandalized_sign_w_count = character_count(vandalized_sign)
  vandalized_sign_w_count.each do |k, v|

    return false unless normal_sign_w_count.key?(k.downcase)
    return false if v > normal_sign_w_count[k]
  end
  true
end

def character_count(str)
  char_count = Hash.new(0)
  str.gsub(/\W/, "").downcase.chars do |char|
    char_count[char] += 1
  end
  char_count
end
